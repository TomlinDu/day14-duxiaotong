package oocl.afs.todoList.repository;

import oocl.afs.todoList.entity.Todo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class InMemoryTodoRepository {
    List<Todo> todos = new ArrayList<>();

    public List<Todo> getTodos() {
        return todos;
    }


}
