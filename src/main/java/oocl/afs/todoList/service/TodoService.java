package oocl.afs.todoList.service;

import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.repository.TodoRepository;
import oocl.afs.todoList.service.dto.TodoRequest;
import oocl.afs.todoList.service.dto.TodoResponse;
import oocl.afs.todoList.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        List<Todo> todoList = todoRepository.findAll();
        return todoList.stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse create(Todo todo) {
        return TodoMapper.toResponse(todoRepository.save(todo));
    }

    public void delete(Long id) {
        todoRepository.deleteById(id);
    }

    public TodoResponse update(Long id, TodoRequest todoRequest) {
        Optional<Todo> findTodo = todoRepository.findById(id);
        Todo toBeUpdatedEmployee = findTodo.get();
        if (todoRequest.getName() != null) {
            toBeUpdatedEmployee.setName(todoRequest.getName());
        }
        toBeUpdatedEmployee.setDone(todoRequest.isDone());
        Todo savedTodo = todoRepository.save(toBeUpdatedEmployee);
        return TodoMapper.toResponse(savedTodo);
    }


}
