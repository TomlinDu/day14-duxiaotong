package oocl.afs.todoList.service.mapper;

import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.service.dto.TodoRequest;
import oocl.afs.todoList.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

import java.util.Optional;

public class TodoMapper {
    private TodoMapper() {
    }

    public static Todo toEntity(TodoRequest request) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(request, todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse response = new TodoResponse();
        BeanUtils.copyProperties(todo, response);
        return response;
    }
}
