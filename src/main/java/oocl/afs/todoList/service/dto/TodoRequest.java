package oocl.afs.todoList.service.dto;

public class TodoRequest {
    private String name;
    private boolean done;

    public TodoRequest(String name, boolean done) {
        this.name = name;
        this.done = done;
    }

    public TodoRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
