package oocl.afs.todoList.controller;

import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.exception.TodoNotFoundException;
import oocl.afs.todoList.service.TodoService;
import oocl.afs.todoList.service.dto.TodoRequest;
import oocl.afs.todoList.service.dto.TodoResponse;
import oocl.afs.todoList.service.mapper.TodoMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getAllTodos() {
        return todoService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoRequest request) {
        return todoService.create(TodoMapper.toEntity(request));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id) {
        todoService.delete(id);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable Long id, @RequestBody TodoRequest todoRequest) {
        return todoService.update(id, todoRequest);
    }

}
