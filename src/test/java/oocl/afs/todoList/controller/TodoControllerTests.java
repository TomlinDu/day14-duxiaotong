package oocl.afs.todoList.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.repository.TodoRepository;
import oocl.afs.todoList.service.dto.TodoRequest;
import oocl.afs.todoList.service.mapper.TodoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        TodoRequest request = getTodoRequestStudy();
        Todo savedTodo = todoRepository.save(TodoMapper.toEntity(request));

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(request.isDone()));
    }

    @Test
    void should_create_todo() throws Exception {
        TodoRequest request = getTodoRequestStudy();
        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequest = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(request.isDone()));
    }

    @Test
    void should_delete_todo_by_id() throws Exception {
        TodoRequest request = getTodoRequestStudy();
        Todo savedTodo = todoRepository.save(TodoMapper.toEntity(request));

        mockMvc.perform(delete("/todo/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoRepository.findById(savedTodo.getId()).isEmpty());
    }

    @Test
    void should_switch_todo_done() throws Exception {
        Todo previousTodo = new Todo(null, "read", false);
        Todo savedTodo = todoRepository.save(previousTodo);

        TodoRequest todoUpdateRequest = new TodoRequest("read",true);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(todoUpdateRequest);
        mockMvc.perform(put("/todo/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Optional<Todo> optionalTodo = todoRepository.findById(savedTodo.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        assertEquals(savedTodo.getId(), updatedTodo.getId());
        assertEquals(todoUpdateRequest.getName(), updatedTodo.getName());
        assertEquals(todoUpdateRequest.isDone(), updatedTodo.isDone());
    }


    private static TodoRequest getTodoRequestStudy() {
        TodoRequest request = new TodoRequest();
        request.setName("Study React");
        request.setDone(false);
        return request;
    }

}
